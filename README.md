# Zadatak
Korišćenjem Microsoft Visual C++ razvojnog okruženja realizovati primer aplikacije koja implementira funkcionalnost POP3 email servera. Aplikacija očekuje zahteve za uspostavom veze na unapred zadatom TCP portu. Aplikacija takođe treba da omoguće da se nekim korisnicima zabrani pristup serveru. Korisnici se identifikuju e-mail adresom koja se šalje preko POP3 poruke „USER“. Aplikaciju testirati korišćenjem e-mail klijenta. Promenu stanja serverske strane realizovati korišćenjem jezgra komunikacione programske podrške. 

# Koncept rešenja
Rešenju se pristupilo prvo kreirajući SDL i MSC dijagrame kako bi se pojasnila logika i da bi implementacija bila lakša.
![](https://i.imgur.com/osbUqY5.png)
![](https://i.imgur.com/xW4V9Oy.png)

**SDL Dijagram**

![](https://i.imgur.com/HTOz4IF.png)

**MSC Dijagram**

# Opis rešenja
Prilikom pokretanja programa prvo se instancira objekat klase FSMSystem i poziva se nit čije izvršavanje traje sve dok server automat ne postavi promenljivu g_ProgramEnd na True. Takođe, prilikom pokretanja servera, otvara se nov socket na unapred zadatom portu . Stanja automata se menjaju SetState() funkcijom dok se upravljanje slanjem i primanjem poruka vrši InitEventProc() funkcijom. Za samo slanje poruka server automat je kodiran u odnosu na SDL dijagram.
Po startovanja servera, očekuje se sa klijentske strane da stigne poruka sa korisničkim imenom, ako se poklapa sa bazom korisnika na serverskoj strani, šalje se potvrdna poruka i klijent šalje šifru tog korisnika. Nakon istog procesa evaluacije, server javlja klijentu da je autorizacija uspešna i šalje informaciju o trenutnom broju neposlatih mejlova i njihovoj ukupnoj veličini. Klijent izdaje zahtev o primanju svih raspoloživih mejlova nakon čega ih server, jedan po jedan, šalje nazad korisniku. Čim se mejl pošalje, isti se briše na serverskoj strani – stanje FSM_Sr_Deleting. (što je i jedna od ključnih osobina POP3 email protokola). Nakon ovog procesa, prvo klijent gasi konekciju i zatvara socket slanjem quit poruke pa potom i server. Gašenje servera je realizovano u stanju FSM_Sr_Disconnect().

# Testiranje
*  Testiranje je realizovano korišćenjem dva računara, gde se na jednom računaru pokretao POP3 klijent, a na drugom POP3 server. 
*  Komunikacija je praćena u programu Wireshark, gde je bilo neophodno uporediti ostvarenu razmenu poruka, sa priloženom dokumentacijom o POP3 protokolu. 
*  Wireshark je takođe korišćen za proveru enkripcije(sa strane POP3 klijenta) i dekripcije(sa strane POP3 servera) 
*  Testirano je sa fajlovima koji su manji od 255 bajtova.   


# Zaključak
Projekat je namenjen da nam pokaže kako POP3 server i POP3 klijent međusobno komuniciraju, koja pravila se moraju poštovati kada je u pitanju upotreba jezgra komunikacione programske podrške kao i na koji način se pravilno obavlja zaštita komunikacije na bazi enkripcije u ovom slučaju(Vižner metod enkripcije).